### EMST Visualizer

This is a small desktop application based on the Java Swing framework, enabling interactive construction and 
visualization of [undirected weighted graphs](https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)#Weighted_graph)
and demonstration of three well-known algorithms for discovering their 
[euclidean minimum spanning trees](https://en.wikipedia.org/wiki/Euclidean_minimum_spanning_tree) (Prim, Kruskal, and Boruvka).

To run the application, clone or download this repository and execute the `dist/EMST Visualizer.jar` file locally.
Make sure you have a working JRE or JDK (v1.6 or higher) installed on your machine.